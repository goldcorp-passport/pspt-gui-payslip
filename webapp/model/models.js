sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function () {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createManagerModel: function () {
			var oModel = new JSONModel({
				InitAppScore: true,
				ViewScoreBusy:true,
				ViewFinalRender:false
			});
			// oModel.setDefaultBindingMode("TwoWay");
			return oModel;
		}

	};
});