sap.ui.define([], function () {
	"use strict";

	return {
		/**
		 * Rounds the currency value to 2 digits
		 *
		 * @public
		 * @param {string} sValue value to be formatted
		 * @returns {string} formatted currency value with 2 digits
		 */
		currencyValue: function (sValue) {
			if (!sValue) {
				return "";
			}

			return parseFloat(sValue).toFixed(2);
		},

		formatPercent: function (value) {

			return Math.round(value);
			// console.log("value")
		},
		formatState: function (value) {
			var oValueRound = Math.round(value);
			var oRet = "";
			// if (oValueRound <= 79) {
			// 	oRet = "Error"
			// }
			// if (oValueRound >= 80 && oValueRound < 95) {
			// 	oRet = "Warning"
			// }
			// if (oValueRound >= 95) {
			// 	oRet = "Success"
			// }
			if (oValueRound < 85) {
				oRet = "#f03e3e"
			}
			if (oValueRound >= 85 && oValueRound < 95) {
				oRet = "#ffdd00"
			}
			if (oValueRound >= 95) {
				oRet = "#30b32d"
			}
			setTimeout(function () {
				$(".sapMObjectNumberText").css("cssText", "color:" + oRet + " !important");
			}, 500)

			return "Success";
			// console.log("value")
		},
		formatRatingValue: function (value) {
			var oModel = this.getView().getModel("scoreDataModel");
			var Q3 = 5;
			var P4 = parseInt(value); //Math.round(oModel.getProperty("/results/1/Item_Score/BestRef"));
			var P3 = Math.round(oModel.getProperty("/results/1/Item_Score/WorstRef"));

			var Q4 = Q3 - ((P4 - 1) * Q3 / (P3 - 1));

			return Math.round(Q4);
			// parseInt(value) - (())
			// console.log(value)
		},
	};

});