sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"com/gc/scorecards/SampleScoreCards/js/libs/OdataUtility",
	"com/gc/scorecards/SampleScoreCards/js/libs/BaseUtility",
	"com/gc/scorecards/SampleScoreCards/js/libs/GaugeUtility",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/gc/scorecards/SampleScoreCards/model/formatter",
	"com/gc/scorecards/SampleScoreCards/js/libs/gauge",
	"com/gc/scorecards/SampleScoreCards/js/libs/raphael",
	"com/gc/scorecards/SampleScoreCards/js/libs/justgage",
	"com/gc/scorecards/SampleScoreCards/js/libs/underscore"
], function (Controller, JSONModel, OdataUtility, BaseUtility, GaugeUtility, Filter, FilterOperator, formatter) {
	"use strict";
	// com.gc.scorecards.SampleScoreCards
	return Controller.extend("com.gc.scorecards.SampleScoreCards.controller.scorecards", {
		formatter: formatter,
		constructor: function () {

		},
		onInit: function () {
			console.log("init...");
			// var oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			// oRouter.getRoute("Targetscorecards").attachPatternMatched(this._onObjectMatched, this);

		},
		onAfterRendering: function () {
			console.log("after...");
			this._onObjectMatched()
				// this.onceFunction();
		},

		_onObjectMatched: function () {
			console.log("function dummy 2")

			var oManagerModel = this.getView().getModel("configManager");
			if (oManagerModel.getProperty("/InitAppScore")) {
				oManagerModel.setProperty("/InitAppScore", false);
				// console.log()
				var oController = this;
				var oDataUtility = new OdataUtility();

				var url_string = window.location.href
					//"http://www.example.com/t.html?a=1&b=3&c=m2-m3-m4-m5"
					// var url = new URL(url_string);
				console.log(url_string);
				// console.log(url.href);
				// console.log(url.href.indexOf("mtd=01"));
				// console.log(url.href.indexOf("mtd=12"));

				// var getParamUrl = url.searchParams.get("mtd");
				// console.log("->Mnsual?Anual: " + getParamUrl);
				// if (getParamUrl !== null) {
				// 	if (getParamUrl === 12 || getParamUrl === "12") {
				// 		oManagerModel.setProperty("/TituloScore", "Anuales");
				// 	} else {
				// 		oManagerModel.setProperty("/TituloScore", "Mensual");
				// 	}
				// } else {
				// 	getParamUrl = "01";

				// }
				var getParamUrl = "01";
				if (url_string.indexOf("mtd=01") !== -1) {
					oManagerModel.setProperty("/TituloScore", "Mensual");
					console.log(getParamUrl);
					getParamUrl = "01";
					console.log("es 1?")
					console.log(getParamUrl);
				}

				if (url_string.indexOf("mtd=12") !== -1) {
					oManagerModel.setProperty("/TituloScore", "Anuales");
					console.log(getParamUrl);
					getParamUrl = "12";
					console.log("es 12?")
					console.log(getParamUrl);
				}

				var filters = [new Filter("Month", sap.ui.model.FilterOperator.EQ, getParamUrl)];
				var oResultsModelScore = oDataUtility.getModelDetailScores(filters, oController);
				oResultsModelScore
					.then(oController.onCreateModelsInitial.bind(oController))
					.then(oController.onCreateIconTabBar.bind(oController))
					.then(oController.onShowView.bind(oController))
					.catch(oController.showErrorPromise.bind(oController));
			}
		},

		onCreateIconTabBar: function () {
			var oController = this;
			var oBase = new BaseUtility();
			return new Promise(function (resolve, reject) {

				var oModelConfigView = oController.getView().getModel("oModelConfigView");
				var oIconTabBar = oBase.createTabIcon("iconTabBarScore", oController.onSelectTabFilter, "classGrey2");
				var oPage = oController.byId("pageSocreCards");

				oIconTabBar.bindProperty("visible", "configManager>/ViewFinalRender");
				oIconTabBar.setModel(oModelConfigView, "oModelDataIconTab");
				oIconTabBar.bindAggregation("items", "oModelDataIconTab>/oModelIconTabBar", oController.onCreatePanelTabContent.bind(oController));
				oPage.addContent(oIconTabBar);

				console.log("->Created icon tab bar with content panel and tiles<-");
				resolve();

			});

		},
		onCreatePanelTabContent: function (sId, oContext) {
			// console.log(sId, oContext)
			var oController = this;
			var oBase = new BaseUtility();
			var oPanel = oBase.createPanel(oContext.getObject().DataName, "fixMargin");

			/**Agregar correctamente el row de total points en el SP**/

			if (oController.getView().getModel("oModelConfigView").getProperty("/tiles_" + oContext.getObject().DataName)) {
				oPanel.bindAggregation("content", "oModelDataIconTab>/tiles_" + oContext.getObject().DataName, oController.onCreateTile.bind(
					oController));

			}

			var oIconTabFilter = oBase.createIconTabFilter(oContext.getObject().DataName, oContext.getObject().Icon,
				oContext.getObject().DataPrettyName, oPanel);
			console.log("->Created panel´s in tab bar<-");

			return oIconTabFilter;

		},
		onCreateTile: function (sId, oContext) {
			var oController = this;
			var oBase = new BaseUtility();
			var oGauge = new GaugeUtility();
			var oDataUtility = new OdataUtility();
			if (oContext.getObject().DataName.indexOf(" ") !== -1) {
				oContext.getObject().DataName = oContext.getObject().DataName.replace(" ", "");
			}

			var oHBox = oBase.createHBox("hbox" + oContext.getObject().DataName, "hboxClass");
			if (oContext.getObject().TargetRef === "") {
				var textLabel = oContext.getObject().DataValue + " " + oContext.getObject().DataUnit;
				var oLabelT = oBase.createLabel(oContext.getObject().DataName + "LabelT", oContext.getObject().DataValue);
				oLabelT.addStyleClass("vboxClassLabelAloneTop");
				var oLabelB = oBase.createLabel(oContext.getObject().DataName + "LabelB", oContext.getObject().DataUnit);
				oLabelB.addStyleClass("vboxClassLabelAloneBottom");
				var oVBoxR = oBase.createVBox(oContext.getObject().DataName + "-R", "Column", "vboxClassInfo");
				oVBoxR.addItem(oLabelT);
				oVBoxR.addItem(oLabelB);
				oHBox.addItem(oVBoxR);
			} else {
				var oVBoxL = oBase.createVBox(oContext.getObject().DataName + "-L", "Column", "vboxClass");
				var oVBoxR = oBase.createVBox(oContext.getObject().DataName + "-R", "Column", "vboxClassLabel");
				oHBox.addItem(oVBoxL);
				oHBox.addItem(oVBoxR);
			}

			var oPanel = oBase.createPanel(oContext.getObject().DataName + "-P", "panelTile fixMargin", oContext.getObject().DataPrettyName,
				oHBox);

			var oTile = oBase.createCustomTileScoreCard(oPanel, "customClassTile");

			console.log("->Created tiles´s in panel on the tab bar<-");

			if (oContext.getObject().TargetRef === "") {

				var nameClass = "dynamicCSSClas" + oContext.getObject().DataName;

				var hex = oContext.getObject().DataBackgroundColor;
				var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
				var colorRequest = result ? {
					r: parseInt(result[1], 16),
					g: parseInt(result[2], 16),
					b: parseInt(result[3], 16)
				} : null;
				
				var oColorRef = "rgba(" + colorRequest.r + "," + colorRequest.g + "," + colorRequest.b + ", 0.37)!important";
				
				
				// var oColorRef = oContext.getObject().DataBackgroundColor + "5e !important";
				var oColorRef2 = oContext.getObject().DataBackgroundColor + " !important";

				document.styleSheets.item(2).insertRule("." + nameClass + "{ background-color: " + oColorRef +
					";border-bottom: 6px solid " + oColorRef2 + "; }", 0);

				oPanel.addStyleClass(nameClass);
				// var idClass =  this.getView().byId(oContext.getObject().DataName + "-P").getId();
				// console.log("--> " + idClass )

			} else {
				var oContainerGauge = oGauge.createCanvasContentainerGauge(oVBoxL, oVBoxR, oPanel, oContext.getObject().DataName);
				oContainerGauge
					.then(oGauge.createGauge.bind(oController, oDataUtility))
					.catch(oController.showErrorPromise.bind(oController))

			}

			return oTile;

		},
		onSelectTabFilter: function (_oEvent) {
			console.log("->Press icon tab filter: " + _oEvent.getParameters().selectedKey + " <-");
		},

		showErrorPromise: function (results) {
			console.log("-->Error<--");
			console.log(results);
		},

		onCreateModelsInitial: function (_results) {

			var oResults = _results;
			var oController = this;
			return new Promise(function (resolve, reject) {

				/***created core model scorecard***/
				var oModel = new JSONModel();
				oModel.setData(oResults);
				oController.getView().setModel(oModel, "scoreDataModel");

				var oScoreModel = oController.getView().getModel("scoreDataModel");
				// oScoreModel.getProperty("/results").unshift({
				// 	"Header_Score": {
				// 		"Passportid": "10702388",
				// 		"Name": "Lizbeth Zuñiga"
				// 	},
				// 	"Item_Score": {
				// 		"DataPrettyName": "Puntos totales",
				// 		"DataName": "total_points",
				// 		"DataType": "tab"
				// 	},
				// 	"Month": "00"
				// })

				/***created core model icontab and tiles***/

				var oModelConfigView = new JSONModel({
					"oModelIconTabBar": [],
					"oMain": new JSONModel({})
				});
				var oModelInfoMain = new JSONModel({});

				_.map(oScoreModel.getProperty("/results"), function (value) {
					if (value.Item_Score.DataType === "tab") {
						switch (value.Item_Score.DataName) {
						case "people":
							value.Item_Score.Icon = "sap-icon://customer-history";
							break;
						case "production":
							value.Item_Score.Icon = "sap-icon://vehicle-repair";
							break;
						case "property":
							value.Item_Score.Icon = "sap-icon://eam-work-order";
							break;
						default:
							value.Item_Score.Icon = "sap-icon://performance";
							break;
						}
						oModelConfigView.getProperty("/oModelIconTabBar").push(value.Item_Score);
						var tilePropertyName = "tiles_" + value.Item_Score.DataName;
						oModelConfigView.setProperty("/" + tilePropertyName, []);
					}
					if (value.Item_Score.DataType === "main") {
						oModelInfoMain.setProperty("/" + value.Item_Score.DataName, value.Item_Score);
						oModelInfoMain.setProperty("/infoProfile", value.Header_Score);
						// oModelConfigView.getProperty("/oMain").push(value.Item_Score);
						// temp1.
					}
				}.bind(oController));

				oController.getView().setModel(oModelConfigView, "oModelConfigView");
				oController.getView().setModel(oModelInfoMain, "oModelInfoMain");

				_.map(oScoreModel.getProperty("/results"), function (value) {

					if (this.getView().getModel("oModelConfigView").getProperty("/tiles_" + value.Item_Score.DataParentName)) {
						this.getView().getModel("oModelConfigView").getProperty("/tiles_" + value.Item_Score.DataParentName).push(value.Item_Score);
					}
				}.bind(oController));

				console.log("->created initial model<-");
				resolve();
			});

		},
		onShowView: function () {

			var oController = this;
			return new Promise(function (resolve, reject) {
				var oManagerModel = oController.getView().getModel("configManager");
				/*******/
				oManagerModel.setProperty("/ViewFinalRender", true);
				oManagerModel.setProperty("/ViewScoreBusy", false);
				console.log("->End and show view<-")
				resolve();
				/*******/
			});
		},

	});
});